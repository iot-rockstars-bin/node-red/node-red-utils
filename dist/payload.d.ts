export interface Payload<T> {
    payload: T;
    [key: string]: any;
}
export declare function payload<T>(obj: T): Payload<T>;
