# Installation

`yarn add ssh://git@gitlab.com:iot-rockstars-shared/node-red-utils.git#v1.2.0`

The sources of this repo will be automatically created on a push of the [src-repo](https://gitlab.com/iot-rockstars-shared/node-red-utils-src/-/tree/master)